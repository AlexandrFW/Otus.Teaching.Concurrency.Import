﻿namespace Otus.Teaching.Concurrency.Import.Core.Helpers
{
    public class RangeData
    {
        public int Length { get; set; }
        public int MinRangeVal { get; set; }
        public int MaxRangeVal { get; set; }
        public int CurrentValue { get; set; } 
    }
}