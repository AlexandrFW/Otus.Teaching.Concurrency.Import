﻿namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
    public interface IAppDbContextFactory<T> 
    {
        public T Create(string connectionString);
    }
}