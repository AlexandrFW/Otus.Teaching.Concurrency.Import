﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Factories;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public static class DataLayerIjector
    {
        public static IServiceCollection AddDataAccessLayer(this IServiceCollection services)
        {
            return services
                .AddScoped<IAppDbContextFactory<ApplicationDbContext>, AppDbContextFactory>()
                .AddScoped<ICustomerRepository, CustomerRepository>()
                .AddScoped<IDataParser<Customer>, CsvParser>();
        }
    }
}