﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Factories
{
    internal class AppDbContextFactory : IAppDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext Create(string connectionString)
        {
            var options = new DbContextOptionsBuilder().UseSqlServer(connectionString).Options;
            return new ApplicationDbContext(options);
        }
    }
}