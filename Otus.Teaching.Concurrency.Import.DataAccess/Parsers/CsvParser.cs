﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Serializers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<Customer>
    {
        IConfiguration _config;

        public CsvParser(IConfiguration config)
        {
            _config = config;            
        }

        public Customer Parse(string data)
        {
            return CustomCsvSerializer.DeserializeObject<Customer>(data, _config.GetSection("Delimeter").Value);
        }
    }
}