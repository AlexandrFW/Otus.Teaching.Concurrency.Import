using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    internal class CustomerRepository : ICustomerRepository
    {
        IConfiguration _configuration;

        IAppDbContextFactory<ApplicationDbContext> _dbContextFactory;

        public CustomerRepository(IAppDbContextFactory<ApplicationDbContext> dbContextFactory, 
                                  IConfiguration configuration)
        {
            _configuration = configuration;
            _dbContextFactory = dbContextFactory;
        }

        public int AddCustomer(Customer customer)
        {
            using (var db = _dbContextFactory.Create(_configuration.GetConnectionString("CustomersDb")))
            {
                db.Customers.Add(customer);
                return db.SaveChangesAsync().Result;
            }
        }
    }
}