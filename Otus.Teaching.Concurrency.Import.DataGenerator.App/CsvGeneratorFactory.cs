﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public static class CsvGeneratorFactory
    {
        public static IDataGenerator GetCsvGenerator(IConfiguration config)
        {
            return new CsvGenerator(config);
        }
    }
}