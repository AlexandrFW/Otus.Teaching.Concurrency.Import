﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    class Program
    {
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.csv");

        private static IConfiguration _configuration;

        private static IServiceProvider _serviceProvider;

        static void Main(string[] args)
        {            
            Console.WriteLine("Generating csv data...");

            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            BuildConfiguation();

            ConfigureDI();

            var generator = CsvGeneratorFactory.GetCsvGenerator(_configuration);
            generator.Generate();

            Console.WriteLine($"CSV data file in {_dataFilePath} has been generated");
        }

        private static void BuildConfiguation()
        {
            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();
        }

        static void ConfigureDI()
        {
            _serviceProvider = new ServiceCollection()
                .AddSingleton(_configuration)
                .BuildServiceProvider();
        }
    }
}