﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;

namespace Otus.Teaching.Concurrency.Import.DataGenerator
{
    public static class GeneratorInjector
    {
        public static IServiceCollection AddGenerators(this IServiceCollection collection)
        {
            return collection.AddScoped<IDataGenerator, CsvGenerator>();
        }
    }
}