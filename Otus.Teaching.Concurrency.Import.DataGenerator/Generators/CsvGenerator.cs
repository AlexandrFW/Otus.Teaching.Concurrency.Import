﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.DataGenerator.Serializers;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using System;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        IConfiguration _config;

        private readonly string _fileName;
        private readonly int _dataCount;

        public CsvGenerator(IConfiguration config)//string fileName, int dataCount)
        {
            _config = config;

            _fileName = _config.GetSection("FileName").Value;
            if (string.IsNullOrEmpty(_fileName) )
            {
                _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.csv");
            }
            _dataCount = Convert.ToInt32(_config.GetSection("CountRowToGenerate").Value);
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using var stream = File.Create(_fileName);

            var serializedCustomersList = CustomCsvSerializer.SerializeObjectList(customers, ";");

            var bytesToWrite = Encoding.UTF8.GetBytes(serializedCustomersList);

            stream.Write(bytesToWrite, 0, bytesToWrite.Length);
        }
    }
}