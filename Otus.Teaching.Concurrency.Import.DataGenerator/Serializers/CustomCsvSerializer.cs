﻿using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Serializers
{
    public static class CustomCsvSerializer
    {
        public static string SerializeObject<T>(T objectToSerialize, string delimeter)
        {
            if (objectToSerialize == null)
                throw new ArgumentNullException("Передаваемы объект не должен быть NULL");

            var name = objectToSerialize.GetType().Name;
            Type type = objectToSerialize.GetType();
            var fields = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

            var resultBuilder = new StringBuilder();
            resultBuilder.Append("name");
            resultBuilder.Append(delimeter);

            foreach (var field in fields)
            {
                resultBuilder.Append(field.Name);
                resultBuilder.Append(delimeter);
            }

            resultBuilder.Append("\r\n");

            resultBuilder.Append(name);
            resultBuilder.Append(delimeter);

            foreach (var field in fields)
            {
                resultBuilder.Append($"{field.GetValue(objectToSerialize)}");
                resultBuilder.Append(delimeter);
            }

            resultBuilder.Append("\r\n");

            return resultBuilder.ToString();
        }

        public static string SerializeObjectList<T>(List<T> objectToSerializeList, string delimeter)
        {
            if (objectToSerializeList == null)
                throw new ArgumentNullException("Передаваемый объект не должен быть NULL");

            var name = objectToSerializeList[0]!.GetType().Name;
            Type type = objectToSerializeList[0]!.GetType();
            var fields = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

            var resultBuilder = new StringBuilder();
            resultBuilder.Append("name");
            resultBuilder.Append(delimeter);

            foreach (var field in fields)
            {
                resultBuilder.Append(field.Name);
                resultBuilder.Append(delimeter);
            }

            resultBuilder.Append("\r\n");

            foreach (var item in objectToSerializeList)
            {
                resultBuilder.Append(item!.GetType().Name);
                resultBuilder.Append(delimeter);

                foreach (var field in fields)
                {
                    resultBuilder.Append($"{field.GetValue(item)}");
                    resultBuilder.Append(delimeter);
                }
                resultBuilder.Append("\r\n");
            }

            resultBuilder.Append("\r\n");

            return resultBuilder.ToString();
        }

        public static List<T> DeserializeObjectList<T>(string serializedObjectByCsv, string delimeter)
        {
            if (string.IsNullOrEmpty(serializedObjectByCsv))
                throw new ArgumentNullException("Проверьте сериализованную строку. Строка не должна быть пустой");

            var type = typeof(T);

            var objectName = type.Name;

            var sliptSerializedObjectArray = serializedObjectByCsv.Split("\r\n");

            var splitFildsNamesArray = sliptSerializedObjectArray[0].Split(";");

            var listF = new List<T>();

            if (sliptSerializedObjectArray.Length > 1)
            {
                for (int i = 1; i < sliptSerializedObjectArray.Length - 1; i++)
                {
                    var splitValuesArray = sliptSerializedObjectArray[i].Split(";");

                    if (objectName == splitValuesArray[0])
                    {
                        var inctance = (T)Activator.CreateInstance(typeof(T))!;

                        for (int j = 1; j < splitValuesArray.Length - 1; j++)
                        {
                            var name = type.GetField(splitFildsNamesArray[j], BindingFlags.Instance | BindingFlags.NonPublic);
                            name?.SetValue(inctance, int.Parse(splitValuesArray[j]));
                        }

                        listF.Add(inctance);
                    }
                }
            }

            return listF;
        }

        public static T DeserializeObject<T>(string serializedObjectByCsv, string delimeter)
        {
            if (string.IsNullOrEmpty(serializedObjectByCsv))
                throw new ArgumentNullException("Проверьте сериализованную строку. Строка не должна быть пустой");

            var type = typeof(T);

            var objectName = type.Name;

            var sliptSerializedObjectArray = serializedObjectByCsv.Split("\r\n");

            var splitFildsNamesArray = sliptSerializedObjectArray[0].Split(";");

            if (sliptSerializedObjectArray.Length > 1)
            {
                for (int i = 1; i <= sliptSerializedObjectArray.Length - 1; i++)
                {
                    var splitValuesArray = sliptSerializedObjectArray[i].Split(";");

                    if (objectName == splitValuesArray[0])
                    {
                        var inctance = (T)Activator.CreateInstance(typeof(T));

                        for (int j = 1; j < splitValuesArray.Length - 1; j++)
                        {
                            var name = type.GetField(splitFildsNamesArray[j], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

                            int id = 0;
                            if (int.TryParse(splitValuesArray[j], out id))
                                name?.SetValue(inctance, id);
                            else
                                name?.SetValue(inctance, splitValuesArray[j]);
                        }

                        return inctance;
                    }
                }
            }

            throw new NullReferenceException("Exception");
        }
    }
}