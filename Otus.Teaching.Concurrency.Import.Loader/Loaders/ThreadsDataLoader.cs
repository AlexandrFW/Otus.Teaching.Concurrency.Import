﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.IO;
using System;
using System.Linq;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Threading;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Helpers;
using System.Diagnostics;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class ThreadsDataLoader : IDataLoader
    {
        private readonly IConfiguration _config;
        ICustomerRepository _repository;
        ILogger<ThreadsDataLoader> _logger;
        IDataParser<Customer> _csvParser;

        private readonly string _fileName;
        private readonly int _threadsCount;

        private int _rowLines;

        private CountdownEvent countDown;

        private string[] fileData;

        private bool _isThreadPoolUsed = false;

        public ThreadsDataLoader(ILogger<ThreadsDataLoader> logger,
                                 IConfiguration config, 
                                 ICustomerRepository repository,
                                 IDataParser<Customer> csvParser) 
        {
            _logger = logger;
            _config = config;
            _repository = repository;
            _csvParser = csvParser;

            _isThreadPoolUsed = _config.GetSection("UseThreadPool").Value == "1" ? true : false;
            _threadsCount = Convert.ToInt32(_config.GetSection("ThreadsCount").Value);
            _fileName = _config.GetSection("FileName").Value;
            if (string.IsNullOrEmpty(_fileName))
            {
                _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.csv");
            }

            countDown = new CountdownEvent(_threadsCount);
        }

        public void LoadData()
        {
            _logger.LogInformation("Start loading data...");

            fileData = File.ReadLines(_fileName).Where(x => x.Length > 2).ToArray();

            _rowLines = fileData.Count();

            _logger.LogInformation($"Row lines in file = {_rowLines}");

            if (fileData.Length > 0)
            {
                var firstRowIndex = 1;
                var rangeLength = _rowLines / _threadsCount;

                _logger.LogInformation($"Each range should contain approximately {rangeLength} records");

                var stopwatch = new Stopwatch();
                stopwatch.Start();

                for (int i = 1; i <= _threadsCount; i++)
                {
                    var endOfRange = firstRowIndex + rangeLength;

                    if (endOfRange > _rowLines)
                        endOfRange = _rowLines - 1;

                    var range = new RangeData();
                    range.MinRangeVal = firstRowIndex;
                    range.MaxRangeVal = endOfRange;

                    if (_isThreadPoolUsed)
                    {
                        ThreadPool.QueueUserWorkItem((state) => SaveCustomer(range));
                    }
                    else
                    {
                        var thread = new Thread(new ParameterizedThreadStart(SaveCustomer));
                        thread.Name = $"thread_{i}";
                        thread.IsBackground = true;
                        thread.Start(range);
                    }

                    firstRowIndex = endOfRange + 1;
                }

                countDown.Wait();

                stopwatch.Stop();

                _logger.LogInformation($"Время, затраченное на обработку через {(_isThreadPoolUsed ? "ThreadPool" : "Threads")} = {stopwatch.ElapsedMilliseconds}ms");

                _logger.LogInformation("All data has been loaded");

                Console.ReadKey();
            }
        }

        private void SaveCustomer(object rangeData)
        {
            var range = (RangeData)rangeData;
            int min = range.MinRangeVal;
            int max = range.MaxRangeVal;

            Console.WriteLine($"Thread: {Thread.CurrentThread.Name}; Range: Min[{min}], Max[{max}]");

            var headerLine = fileData[0];

            for (int i = min; i <= max; i++)
            {
                range.CurrentValue = i;

                var readLine = $"{headerLine}\r\n{fileData[i]}";
                var customer = _csvParser.Parse(readLine);

                if (customer != null)
                {
                    _logger.LogInformation($"Thread: {Thread.CurrentThread.Name}; Try to save customer in the DB");

                    var result = _repository.AddCustomer(customer);
                    _logger.LogInformation($"Saving data result {result}");
                }
                else
                {
                    _logger.LogError($"Cannot deserialize object[{range.CurrentValue}]");
                }

                _logger.LogWarning($"Range current value = {range.CurrentValue}");
            }

            countDown.Signal();
        }
    }
}