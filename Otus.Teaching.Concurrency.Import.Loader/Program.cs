﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataGenerator;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.csv");

        private static bool _startDataGeneratorAsProcess = false;

        private static IConfiguration _configuration;

        private static IServiceProvider _serviceProvider;

        private static ILogger<Program> _logger;

        static void Main(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            BuildConfiguation();

            ConfigureDI();

            _logger = _serviceProvider.GetService<ILoggerFactory>()
                                      .CreateLogger<Program>();

            _logger.LogDebug("Starting application");
            _logger.LogInformation($"Loader started with process Id {Process.GetCurrentProcess().Id}...");
            _logger.LogInformation($"{_configuration.GetSection("CountRowToGenerate").Value} customers should be generated");
            _logger.LogInformation($"Generate customers data...");

            _startDataGeneratorAsProcess = _configuration.GetSection("StartDataGeneratorAsProcess").Value == "1" ? true : false;

            if (_startDataGeneratorAsProcess)
            {
                GenerateCustomersDataFileViaProcess();
            }
            else
            {
                GenerateCustomersDataFile();
            }

            var loader = _serviceProvider.GetService<IDataLoader>(); 
            loader.LoadData();
        }

        private static void BuildConfiguation()
        {
            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();
        }

        static void GenerateCustomersDataFile()
        {
            var csvGenerator = _serviceProvider.GetService<IDataGenerator>();
            csvGenerator.Generate();
        }

        static void GenerateCustomersDataFileViaProcess()
        {
            var processInfo = new ProcessStartInfo
            {
                FileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                                        "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe")
            };

            var process = new Process();
            process.StartInfo = processInfo;
            process.Start();
            process.WaitForExit();

            while (!process.HasExited)
            {
                _logger.LogWarning("Wait while CSV data file will be generated");
                Task.Delay(200).Wait();
            }

            _logger.LogInformation("CSV data file has been generated. Process has been terminated");

            process.Dispose();
        }

        static void ConfigureDI()
        {
            _serviceProvider = new ServiceCollection()
                .AddSingleton(_configuration)
                .AddLogging(c => c.AddConsole(opt => opt.LogToStandardErrorThreshold = LogLevel.Debug))
                .AddDataAccessLayer()
                .AddGenerators()
                .AddScoped<IDataLoader, ThreadsDataLoader>()
                .BuildServiceProvider();
        }
    }
}